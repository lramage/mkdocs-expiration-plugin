# Mkdocs Expiration Plugin

## Synopsis

If the published date is older than a predefined interval, then `mkdocs build`
should fail, and a warning message issues for the offending document.

For example, here is a snippet from an article:

```md
---
date: 2001-07-19
---

# Article Title
```

And here is the option in the `mkdocs.yml` file:

```yml
extra:
  expiration_period: 2Y
```

Then during the build phase:

```sh
mkdocs build
Error: documentation has not been update since July, 2001.
```

## Acknowledgement

The [project icon](https://commons.wikimedia.org/wiki/File:Expired.svg) is attributed to [RRZEicons](https://commons.wikimedia.org/wiki/User:RRZEicons)
and is licensed under a [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en) license. 

## License

SPDX-License-Identifier: [MIT](https://spdx.org/licenses/MIT)
